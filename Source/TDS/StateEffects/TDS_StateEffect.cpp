// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS/Character/TDSHealthComponent.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;
	
	
	//UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect::InitObject"));

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}


return true;
}

void UTDS_StateEffect::DectroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	
	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DectroyObject()
{
	Super::DectroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	
	if(myActor)
	{
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DectroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	
	Super::InitObject(Actor);
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DectroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTimer, true);

	if(ParticleEffect)
	{
		ITDS_IGameActor* myInterFace = Cast<ITDS_IGameActor>(myActor);
		if (myInterFace)
		{
			FName NameBoneToAttach = myInterFace->BoneToAttach;
			FVector Loc = myInterFace->OffsetLocationByRootComponent;
//			UE_LOG(LogTemp, Warning, TEXT("BoneName = %s. Offset = %s."), *NameBoneToAttach.ToString(), *Loc.ToString());


			ParticleEmmiter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		
		}
	}
	return true;
}

void UTDS_StateEffect_ExecuteTimer::DectroyObject()
{
	if (ParticleEmmiter)
	{
		ParticleEmmiter->DestroyComponent();
	}
	
	ParticleEmmiter = nullptr;

	Super::DectroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		
		UTDSHealthComponent* myHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
