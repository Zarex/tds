// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS/Weapons/DropedItemDefault.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Components/PrimitiveComponent.h"


// Sets default values
ADropedItemDefault::ADropedItemDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	RootComponent = ItemStaticMesh;
 	ItemStaticMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
;
 	ItemStaticMesh->Mobility = EComponentMobility::Movable;

	ItemStaticMesh->SetSimulatePhysics(true);

}

// Called when the game starts or when spawned
void ADropedItemDefault::BeginPlay()
{
	Super::BeginPlay();
	
}
void ADropedItemDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	LifeTick(DeltaTime);
}

void ADropedItemDefault::InitShell(FWeaponShell InitParam)
{
 	ItemStaticMesh->SetStaticMesh(InitParam.ShellMesh);
	
	FVector ImpulseDir = this->GetActorForwardVector() * InitParam.DropShellDirection.X + this->GetActorRightVector() * InitParam.DropShellDirection.Y + this->GetActorUpVector() * InitParam.DropShellDirection.Z;


	ItemStaticMesh->AddImpulse(ImpulseDir * InitParam.DropShellImpulsePower);

	
	LifeTimer= InitParam.ShellLifeTime;
	ShellSetting = InitParam;
}

void ADropedItemDefault::InitMagazine(FWeaponMagazine InitParam)
{
	ItemStaticMesh->SetStaticMesh(InitParam.MagazineMesh);

	FVector ImpulseMagDir = this->GetActorForwardVector() * InitParam.DropMagazineDirection.X + this->GetActorRightVector() * InitParam.DropMagazineDirection.Y + this->GetActorUpVector() * InitParam.DropMagazineDirection.Z;
  	
	ItemStaticMesh->AddImpulse(ImpulseMagDir * InitParam.DropMagazineImpulsePower);
 
	

	LifeTimer = InitParam.MagazineLifeTime;
	MagazineSetting = InitParam;
}

void ADropedItemDefault::LifeTick(float DeltaTime)
{
	if (LifeTimer < 0.0f)
	{
		this->Destroy(true);
	}
	else
	{
		LifeTimer -= DeltaTime;
	}
}

