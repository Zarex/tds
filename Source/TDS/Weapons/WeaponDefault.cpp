// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Character.h"
#include "Animation/AnimInstance.h"
#include "TDS/Weapons/DropedItemDefault.h"
#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS/Structure/TDS_EnvironmentStructure.h"
#include "TDS/Character/TDSInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("No Collision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);

}

void AWeaponDefault::FireTick(float DeltaTime)
{
		if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading )
		{
			if (FireTimer < 0.f)
				Fire();
			else 
				FireTimer -= DeltaTime;

			OnWeaponFireEnd.Broadcast();
		}
 		else
		{
			if(FireTimer > 0.0f)
 				FireTimer -= DeltaTime;
 			else
 				FireTimer = -0.1f;
		}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
		
	}
	
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFIre())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
}

bool AWeaponDefault::CheckWeaponCanFIre()
{
	return !BlockFire;
}
FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	
 
 	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
 	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());
	
	UAnimMontage* FireAnimToPlay = nullptr;;
	if (WeaponAiming)
	{
		FireAnimToPlay = WeaponSetting.WeaponAnimation.CharAimFire;
		
	}
	else
	{
		FireAnimToPlay = WeaponSetting.WeaponAnimation.CharFire;
		
	}

	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShot();

	OnWeaponFireStart.Broadcast(FireAnimToPlay);
	

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		
		if (WeaponSetting.WeaponAnimation.WeaponFire)
			{
			UE_LOG(LogTemp, Warning, TEXT("PLAYING Fire ANIM MONTAGE In Weapons"));
			SkeletalMeshWeapon->PlayAnimation(WeaponSetting.WeaponAnimation.WeaponFire,false);
			}

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
				//ballistic fire

				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myProjectile)
				{
					
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				
				FHitResult Hit;
				TArray<AActor*>	Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(),SpawnLocation,EndLocation*WeaponSetting.DistanceTrace,ETraceTypeQuery::TraceTypeQuery4,false,Actors,EDrawDebugTrace::ForDuration,Hit,true,FLinearColor::Red,FLinearColor::Green,5.0f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];
						
						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial,FVector(20.0f),Hit.GetComponent(),NAME_None,Hit.ImpactPoint,Hit.ImpactNormal.Rotation(),EAttachLocation::KeepWorldPosition,10.0f);
							
						}
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}
					if (WeaponSetting.ProjectileSetting.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
					}


					ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(Hit.GetActor());
					if (myInterface)
					{
						FName MyBone = Hit.BoneName;
						if (MyBone != "None")
						{
							myInterface->BoneToAttach = MyBone;
							myInterface->OffsetLocationByRootComponent = FVector(0.f);
						}
						else
						{
							ATDS_EnvironmentStructure* HitActor = Cast<ATDS_EnvironmentStructure>(Hit.GetActor());
							if (HitActor)
							{
								myInterface->BoneToAttach = FName("None");
								myInterface->OffsetLocationByRootComponent = HitActor->ParticleOffsetByRootComponentToEffect;

							}
						}

					}

				
					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype);
					

					//UTDS_StateEffect* NewEffect = NewObject<UTDS_StateEffect>(Hit.GetActor(), FName("Effect"));
					
					UGameplayStatics::ApplyPointDamage(Hit.GetActor(),WeaponSetting.ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(),this,NULL);

				}
			
			}
		}
		EjectAmmo();
		OnWeaponFireEnd.Broadcast();
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if(CheckCanWeaponReload())
			InitReload();
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		WeaponAiming = true;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		WeaponAiming = false;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		WeaponAiming = false;
		BlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.0f);

}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.0f);
	
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		
	if (ShowDebug)
 			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		
		if (ShowDebug)
 			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		


		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		


		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
		

		//DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}



class UAnimMontage* AWeaponDefault::GetReloadMontage()
{
	return WeaponAnimation.WeaponReload;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	if (!WeaponReloading) 
	{
		WeaponReloading = true;

		ReloadTimer = WeaponSetting.ReloadTime;



		EjectMagazine();

		//Anim reload

		//Char reload
		UAnimMontage* AnimToPlay = nullptr;;
		if (WeaponAiming)
			AnimToPlay = WeaponSetting.WeaponAnimation.CharAimReload;
		
		else
			AnimToPlay = WeaponSetting.WeaponAnimation.CharReload;
	

		OnWeaponReloadStart.Broadcast(AnimToPlay);

		//Weapon reload
			if (WeaponSetting.WeaponAnimation.WeaponReload)
			{
				
 				SkeletalMeshWeapon->PlayAnimation(WeaponSetting.WeaponAnimation.WeaponReload,false);
 			}

	}
	
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int32 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int32 AmmoNeedTakeFromInv;
	int32 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int32 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
				MyInv->OnWeaponNotHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
			else
			{
				MyInv->OnWeaponHaveRound.Broadcast(MyInv->GetWeaponIndexSlotByName(IdWeaponName));
			}
		}
	}

	return result;
}

int32 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

void AWeaponDefault::EjectAmmo()
{
	const USkeletalMeshSocket* EjectAmmoSoc = SkeletalMeshWeapon->GetSocketByName("AmmoEject");
	if(EjectAmmoSoc)
	{
  		FTransform Offset = WeaponSetting.WeaponShell.ShellTransform;
		FVector DropLocation = EjectAmmoSoc->GetSocketLocation(SkeletalMeshWeapon);

		FTransform NewTransform;

		NewTransform.SetLocation(DropLocation);
		NewTransform.SetScale3D(Offset.GetScale3D());
		NewTransform.SetRotation((GetActorRotation()+Offset.Rotator()).Quaternion());

	
		FActorSpawnParameters DropSpawnParams;
		DropSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		DropSpawnParams.Owner = this;

		ADropedItemDefault* NewDropedItem = GetWorld()->SpawnActor<ADropedItemDefault>(DropedItem, NewTransform, DropSpawnParams);
	
		if(NewDropedItem)
		{
			NewDropedItem->InitShell(WeaponSetting.WeaponShell);
 		}
	}
}

void AWeaponDefault::EjectMagazine()
{
	const USkeletalMeshSocket* EjectMagSoc = SkeletalMeshWeapon->GetSocketByName("AmmoMesh");

	if(EjectMagSoc)
	{
		FTransform Offset = WeaponSetting.WeaponMagazine.MagazineTransform;
		FVector DropLocation = EjectMagSoc->GetSocketLocation(SkeletalMeshWeapon);
	
		FTransform NewTransform;

		NewTransform.SetLocation(DropLocation);
		NewTransform.SetScale3D(Offset.GetScale3D());
		NewTransform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());


		FActorSpawnParameters DropSpawnParams;
		DropSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		DropSpawnParams.Owner = this;

		ADropedItemDefault* NewDropedItem = GetWorld()->SpawnActor<ADropedItemDefault>(DropedItem, NewTransform, DropSpawnParams);

		if (NewDropedItem)
		{
			NewDropedItem->InitMagazine(WeaponSetting.WeaponMagazine);
		}
	}

}
