// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileDefault.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "TDS/Structure/TDS_EnvironmentStructure.h"
#include "TDS/FuncLibrary/Types.h"
#include "Components/PrimitiveComponent.h"



// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);
	


	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);

	BulletCollisionSphere->bReturnMaterialOnMove = true;

	BulletCollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

// 	BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
// 	BulletSound->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet Projectile Movement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = ProjectileSetting.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = ProjectileSetting.ProjectileInitSpeed;
	
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	if(InitParam.ProjectileMesh)
	{
		BulletMesh->SetStaticMesh(InitParam.ProjectileMesh);
		BulletMesh->SetRelativeTransform(InitParam.ProjectileMeshTransform);
	}
	else
	{
		BulletMesh->DestroyComponent(true);
	}
	if(InitParam.ProjectileFX)
	{
		BulletFX->SetTemplate(InitParam.ProjectileFX);
		BulletFX->SetRelativeTransform(InitParam.ProjectileFXTransform);
	}
	else
	{
		BulletFX->DestroyComponent(true);
	}
	
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileSetting = InitParam;
}

void AProjectileDefault::InitEmptyAmmo(FProjectileInfo InitParam)
{
	
}

void AProjectileDefault::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);
			if (ProjectileSetting.HitDecals.Contains(mySurfacetype))
			{
				UMaterialInterface* myMaterial = ProjectileSetting.HitDecals[mySurfacetype];

				if (myMaterial && OtherComp)
				{
					UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(10.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
					
				}
			}
			if (ProjectileSetting.HitFXs.Contains(mySurfacetype))
			{
				UParticleSystem* myParticle = ProjectileSetting.HitFXs[mySurfacetype];
				if (myParticle)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
				}
			}
			if (ProjectileSetting.HitSound)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.HitSound, Hit.ImpactPoint);
			}

			

			
			ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(Hit.GetActor());
			if (myInterface)
			{
				FName MyBone = Hit.BoneName;
				if (MyBone != "None")
				{
					myInterface->BoneToAttach = MyBone;
					myInterface->OffsetLocationByRootComponent = FVector(0.f);
				}
				else
				{
					ATDS_EnvironmentStructure* HitActor = Cast<ATDS_EnvironmentStructure>(Hit.GetActor());
					if (HitActor)
					{
						myInterface->BoneToAttach = FName ("None");
						myInterface->OffsetLocationByRootComponent = HitActor->ParticleOffsetByRootComponentToEffect;

					}
				}
						
			}

			UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileSetting.Effect, mySurfacetype);

	}

	


	UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileSetting.ProjectileDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
	ImpactProjectile();
	//UGameplayStatics::ApplyRadialDamageWithFalloff()
	//Apply damage cast to if char like bp? //OnAnyTakeDmage delegate
	//UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetOwner()->GetInstigatorController(), GetOwner(), NULL);
	//or custom damage by health component
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}

