// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS/Weapons/Projectiles/ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/FuncLibrary/Types.h"
#include "DrawDebugHelpers.h"


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	
	
	

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	
	if (TimerEnabled)
	{
		TimeToExplose = ProjectileSetting.ExploseDelay;
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}



//  void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
//  {
//  	Super::BulletCollisionSphereHit(HitComp,OtherActor,OtherComp,NormalImpulse,Hit);
//  
//  }

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
	
}

void AProjectileDefault_Grenade::Explose()
{
	MaxRadius = ProjectileSetting.ProjectileMaxRadiusDamage;
	MinRadius = ProjectileSetting.ProjectileMinRadiusDamage;
	Coeff = ProjectileSetting.DamageDownCoeff;

	if (ShowDebug)
	{
		
		DrawDebugSphere(GetWorld(),GetActorLocation(),MaxRadius, 36, FColor::Blue, false, 3.0f);
		DrawDebugSphere(GetWorld(),GetActorLocation(), MinRadius, 36, FColor::Red,false,3.0f);
		UE_LOG(LogTemp, Warning, TEXT("Min Radius = %f , Max Radius = %f"), MinRadius,MaxRadius);
		
	}
	
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		MinRadius,
		MaxRadius,
		Coeff,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}

