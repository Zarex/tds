// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS/Weapons/Projectiles/ProjectileDefault.h"
#include "TDS/FuncLibrary/Types.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

public:

	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

// 	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	
	float MaxRadius;
	float MinRadius;
	float Coeff;

	virtual void ImpactProjectile() override;

	void Explose();

	bool TimerEnabled = false;
	UPROPERTY(BlueprintReadWrite)
	float TimerToExplose = 0.0f;
	UPROPERTY(BlueprintReadWrite)
	float TimeToExplose;

	

};
