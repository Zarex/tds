// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/Types.h"

#include "DropedItemDefault.generated.h"

class AWeaponDefault;

UCLASS()
class TDS_API ADropedItemDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADropedItemDefault();

// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Components")
// 		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = "Components")
		class UStaticMeshComponent* ItemStaticMesh = nullptr;
	UPROPERTY()
		AWeaponDefault* DropItemOwner;

	
	FWeaponShell ShellSetting;
	FWeaponMagazine MagazineSetting;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void InitShell(FWeaponShell InitParam);
	UFUNCTION(BlueprintCallable)
		void InitMagazine(FWeaponMagazine InitParam);

	UFUNCTION()
		void LifeTick(float DeltaTime);

// Timers
	float LifeTimer = 0.0f;
};
