// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS_EnvironmentStructure.generated.h"

UCLASS()
class TDS_API ATDS_EnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDS_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Effects
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UTDS_StateEffect*> Effects;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTDS_StateEffect*> GetAllCurentEffects() override;

	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;

	void AddEffect(UTDS_StateEffect* NewEffect) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	FVector ParticleOffsetByRootComponentToEffect;

};
