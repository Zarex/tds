// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharHealthComponent.h"

void UTDSCharHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield <= 0.0f)
		{
			//FX
			OnShieldEnd.Broadcast();
			UE_LOG(LogTemp, Warning, TEXT("UTDSCharHealthComponent:ChangeHealthValue - Shield < 0"));
		}

	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTDSCharHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;



	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownShieldTimer, this, &UTDSCharHealthComponent::CooldownShieldEnd, CooldownShieldRecoveryTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDSCharHealthComponent::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSCharHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		if (tmp < 0.0f)
		{
			Shield = 0.0f;
		}
		else
		{
			Shield = tmp;
		}
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
