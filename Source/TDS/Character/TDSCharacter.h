// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "GameFramework/SpringArmComponent.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/Character/TDSCharHealthComponent.h"
#include "TDS/StateEffects/TDS_StateEffect.h"
#include "TDS/Interface/TDS_IGameActor.h"
#include "Blueprint/UserWidget.h"
#include "TDSCharacter.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharStunStart);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCharStunEnd);


UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()
protected:

	virtual void BeginPlay() override;

// Inputs

	void InputAxisY(float Value);
	void InputAxisX(float Value);

	void InputAxisMouseWheel(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

// Inventory Inputs

	void TrySwitchNextWeapon();
	void TrySwitchPrevWeapon();

// Ability Inputs

	void TryAbilityEnabled();
	void ResetAbility();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput(Id);
	}

// Inputs End

// Input Flags

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	UPROPERTY(BlueprintReadOnly)
	bool AimEnabled = false;

	bool bIsAlive = true;

	EMovementState MovementState = EMovementState::Run_State;

	AWeaponDefault* CurrentWeapon = nullptr;

	UDecalComponent* CurrentCursor = nullptr;

	TArray<UTDS_StateEffect*> Effects;

	bool bISAbilityEnable = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		float AbilityCooldown = 30.0f;

	UPROPERTY(BlueprintReadWrite)
		float AbilityTimer = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun_Settings")
		bool bIsStun = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun_Settings")
		float StunTime = 5.0f;

	UFUNCTION()
	void CharDead();
	void EnableRagDoll();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;


public:
	ATDSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	UPROPERTY(BlueprintAssignable, Category = "Stun_Settings")
	FOnCharStunStart OnCharStunStart;

	UPROPERTY(BlueprintAssignable, Category = "Stun_Settings")
	FOnCharStunEnd OnCharStunEnd;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	void AbilityTick(float DeltaSeconds);

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
// 	/** Returns CursorToWorld subobject **/
// 	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UTDSCharHealthComponent* HealthComponent;

// Cursor material on decal

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

// Default move rule and state character

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeadAnim")
		TArray<UAnimMontage*> DeadsAmin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDS_StateEffect> AbilityEffect;


private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:


	//Weapon
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;


	//Maximum Height of Camera
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float MaxHeight = 1000.0f;
	//Minimum Height of Camera
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float MinimHeight = 200.0f;
	//The step by which to change the position of the camera in one scroll of the mouse wheel
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float CameraHeightStep = 100.0f;
	UPROPERTY()
		float CurrentHeight;
	UPROPERTY()
		bool SlideDone=true;
	//The speed at which the camera Up/Down move
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float SlideSpeed = 0.005f;
	UPROPERTY()
		float TmpCount;

	UFUNCTION()
		void CameraSlide(float Value);
	UFUNCTION()
		void ProgressSlideUp();
	UFUNCTION()
		void ProgressSlideDown();

	FTimerHandle SlideTimer;
	FTimerHandle StunTimer;



	

	float MouseWheel = 0.0f;

// Tick Function

	UFUNCTION()
		void MovementTick(float DeltaTime);

// Func

		void CharacterUpdate();
		void ChangeMovementState();

		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		void TakeStun();

		void StunOff();

	UFUNCTION()
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	
		void TryReloadWeapon();

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponFireEnd();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

//

		bool TrySwitchWeaponToIndexByKeyInput(int32 ToIndex);
		void DropCurrentWeapon();

	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTDS_StateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int32 CurrentIndexWeapon = 0;

// Interface

	EPhysicalSurface GetSurfaceType() override;

	TArray<UTDS_StateEffect*> GetAllCurentEffects() override;

	void RemoveEffect(UTDS_StateEffect* RemoveEffect) override;

	void AddEffect(UTDS_StateEffect* NewEffect) override;


// End Interface


};

